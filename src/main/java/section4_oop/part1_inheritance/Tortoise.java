package section4_oop.part1_inheritance;

public class Tortoise extends Animal {
    private double maxSpeed = 0.3;
    private int maxAge = 190;


    public Tortoise(String name, int age) {
        super(name, age);
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public int getMaxAge() {
        return maxAge;
    }


    @Override
    public String getMovementType() {
        return "crawl";
    }


}
