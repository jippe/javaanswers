package section4_oop.part1_inheritance;

public class Elephant extends Animal {
    private double maxSpeed = 40;
    private int maxAge = 86;


    public Elephant(String name, int age) {
        super(name, age);
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public int getMaxAge() {
        return maxAge;
    }


    @Override
    public String getMovementType() {
        return "thunder";
    }



}
