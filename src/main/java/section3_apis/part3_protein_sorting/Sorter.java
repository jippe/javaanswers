package section3_apis.part3_protein_sorting;

import java.util.Comparator;

public class Sorter implements Comparator<Protein> {

    @Override
    public int compare(Protein protein, Protein t1) {
        return protein.compareTo(t1);
    }
}
